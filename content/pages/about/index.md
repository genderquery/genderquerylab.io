+++
title = "About"
path = "about"
+++

<figure>
<a class="img-link" href="avery.jpg"><img src="avery.jpg"
width="256" height="256" alt="A light-skinned person of indeterminate gender
with blue hair and white-framed glasses wearing a black blazer over a white
collared shirt."></a>
</figure>

Hello! My name is Avery. I am also known as genderquery. My pronouns are
[they/themself]. I'm a software engineer that strives to make computing
accessible and inclusive for all.

I'm a strong proponent of [free and open source software][foss]. Everything I
create is liberally licensed and can be found on [GitLab] or [GitHub].

I have a [Twitter account under the name @genderquery][twitter] that you are
welcome to follow. Expect dumb jokes.


[they/themself]: https://pronoun.is/they/.../themself
[foss]: https://www.fsf.org/about/what-is-free-software
[GitLab]: https://gitlab.com/genderquery
[GitHub]: https://github.com/genderquery
[twitter]: https://twitter.com/genderquery
[repo]: https://gitlab.com/genderquery/genderquery.gitlab.io
[gitlabpages]: https://about.gitlab.com/product/pages
