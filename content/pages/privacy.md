+++
title = "Privacy Statement"
path = "privacy"
+++

Respecting your privacy is very important to me. This website does not use any
kind of analytics or tracking. No cookies are used. No advertisements are
served.

This website is hosted using [GitLab Pages][gitlab-pages] and I cannot control
what data they track or store. [GitLab's Privacy
Policy][gitlab-privacy-policy] states:

> GitLab also collects potentially personally-identifying information like
> Internet Protocol (IP) addresses from visitors. GitLab does not use such
> information to identify or track individual visitors, however. We collect
> this information to understand how visitors use the Websites, to improve
> performance and content, and to monitor security of the Websites.

The DNS for the domain `averylarsen.com` is controlled by
[Namecheap][namecheap]. [Namecheap's Privacy Policy][namecheap-privacy-policy]
states:

> No personally identifiable information is collected or shared with our DNS
> third party provider for any of our DNS products.

Requests made to and responses from this website are encrypted using a
certificate signed by [Let's Encrypt][lets-encrypt]. Access to this website is
only available using the HTTPS protocol and any unsecured requests made will
be redirected to a HTTPS URI.

A method to contact me regarding privacy questions, concerns, or comments
will be provided in the near-future.


[gitlab-pages]: https://about.gitlab.com/product/pages/
[gitlab-privacy-policy]: https://about.gitlab.com/privacy/
[namecheap]: https://www.namecheap.com
[namecheap-privacy-policy]: https://www.namecheap.com/legal/general/details-for-specific-products-services/
[lets-encrypt]: https://letsencrypt.org/
