+++
title = "Quinoa Tabbouleh"
date = 2019-07-06T04:05:14Z
[taxonomies]
tags = ["quinoa", "tabbouleh", "recipe", "vegetarian", "vegan", "salad", "Middle Eastern"]
categories = ["Food"]
+++

Tabbouleh (pronounced tah‧boo‧lee) is a Middle Eastern salad traditionally made
from finely chopped parsley, tomatoes, mint, olive oil, and lemon juice mixed together
with bulgar. Served cold, it makes an excellent summertime snack or small lunch.

<!-- more -->

<figure>
<a class="img-link" href="tabbouleh.jpg"> <img src="tabbouleh.jpg" width="500"
alt="An up-close shot of Tabbouleh showing large chunks of tomato mixed with
parsley and quinoa."> </a>
<figcaption>Prepared Tabbouleh</figcaption>
</figure>

This variation uses quinoa instead of bulgur, mostly because it was on clearance.
You could also use couscous if so desired. Some other substitutes were made from what I
had on hand and to suit taste. The exact ingredients aren't critical and I
encourage you to be creative and use what you have.

## What you'll need

- 1 cup uncooked quinoa
- 2 cups water
- 1 teaspoon vegetable bouillon
- ½ cup finely chopped parsley (about half a bunch)
- 1 cup tomato wedges (about 4 medium tomatoes)
- 3 tablespoons finely chopped green onions/scallions (about two stalks)
- 2 teaspoons crushed garlic (about two or three cloves)
- 1 teaspoon finely chopped mint leaves (2 to 4 leaves)
- 3 tablespoons good quality olive oil
- 1 tablespoon lemon or lime juice
- salt and pepper to taste

## How to make it

1. Combine the quinoa, water, and bouillon in a pot and cook over medium heat
   until all the liquid is absorbed. I use a rice cooker. It should take about
   30 minutes.
2. Fluff the cooked quinoa and transfer to a container to chill covered in the
   refrigerator. I usually cook the quinoa the day before and let it cool
   overnight.
3. Dump the chilled quinoa into a large bowl and combine all the other
   ingredients, folding gently. Mint can be rather strong, as is the lemon
   juice, so add a little, taste, and add more as needed.
4. Transfer the mixture to a covered container and let sit in the refrigerator
   for a few hours to allow all the flavors to marinate together.
5. Serve cold and enjoy!
