+++
title = "Initial Blog Release"
date = 2019-07-05T21:15:01Z
[taxonomies]
tags = ["blog"]
+++

Welcome to my blog! It's primary purpose is to share some of the things I've
learned while working on projects. My intent is to start a new entry with each
new project. My hope is that I will expose readers to new ideas and reduce
some of the time and frustration involved in learning something new.

<!-- more -->

The site is built using [the static site generator, Zola][zola] and hosted
using [GitLab Pages][gitlab-pages]. I started from scratch with the HTML
templates and the theme is a work in progress.

If you're reading this at the blog's initial release, you will have noticed it
is *very* simple with no CSS or JavaScript. This is intentional! A primary
design goal is to first-most have the website be accessible and highly-usable
with nearly any user-agent, including text-based ones.

I do plan add CSS and some limited JavaScript (for search functionality, for
example), but the goal is to always have the website highly-usable without
them.

An article about making this website is being written, so look out for it. I
also have some drafts on other topics including disassembling old video games
and making an old Mac useful again.

Thanks for taking the time to read this!


[zola]: https://www.getzola.org/
[gitlab-pages]: https://about.gitlab.com/product/pages/
